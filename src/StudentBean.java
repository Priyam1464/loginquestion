public class StudentBean {
    private  int id;
    private String firstName,lastName,username,password,percentage,type;
    public StudentBean(int id,String firstName,String lastName,String percentage,String type)
    {
        this.id=id;
        this.firstName=firstName;
        this.lastName=lastName;
        this.percentage=percentage;
        this.type=type;
    }

    public int getId() {
        return id;
    }

    public String getPercentage() {
        return percentage;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setMarks(int marks) {
        this.percentage = percentage;
    }
    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getType() {
        return type;
    }
}
