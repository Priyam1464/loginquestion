import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginChecker {
    private Connection connection= DatabaseConnection.getConnection();
      public String userLoginSuccessful(String username,String password) throws SQLException {
          String type="default";
          String query="SELECT * FROM Users WHERE username=? AND password=? ";
          PreparedStatement preparedStatement=connection.prepareStatement(query);
          preparedStatement.setString(1,username);
          preparedStatement.setString(2,password);
          ResultSet rs = preparedStatement.executeQuery();
          if(rs.next())
          {
              type=rs.getString("type");
          }
          return type;
      }

}
