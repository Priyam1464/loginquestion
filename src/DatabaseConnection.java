import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static Connection connection=null;
    public static Connection getConnection() {
        if(connection==null)
        {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                connection=  DriverManager.getConnection(
                        "jdbc:oracle:thin:@localhost:1521:XE","system","root");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
