
import java.sql.*;
import java.util.List;

public class StudentDAO {
    private List<StudentBean> studentBeanList;
    private Connection connection= DatabaseConnection.getConnection();
    public List<StudentBean> getStudents() throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Students");
        while(rs.next())
        {
            String firstName=rs.getString("firstname");
            String lastName=rs.getString("lastname");
            String percentage=rs.getString("percentage");
            String type=rs.getString("type");
            int id=rs.getInt("id");
            StudentBean studentBean=new StudentBean(id,firstName,lastName,percentage,type);
            studentBeanList.add(studentBean);
        }
        return studentBeanList;
    }
    public StudentBean getStudentWithId(int id) throws SQLException {
        StudentBean studentBean=null;
        String query="SELECT * FROM Students WHERE id=?";
        PreparedStatement preparedStatement=connection.prepareStatement(query);
       preparedStatement.setInt(1,id);
        // Statement stmt = connection.createStatement();
        ResultSet rs = preparedStatement.executeQuery();
       // System.out.println("rs.next() "+rs.next());
        if(rs.next())
        {
            //System.out.println(" 23 ");
            String firstName=rs.getString("firstname");
            String lastName=rs.getString("lastname");
            String percentage=rs.getString("percentage");
            String type=rs.getString("type");
            studentBean=new StudentBean(id,firstName,lastName,percentage,type);
        }
       // System.out.println("--------"+studentBean.toString());
        return studentBean;
    }
   /* public Boolean loginSuccessful(String username,String password) throws SQLException {
        String query="SELECT * FROM Students WHERE username=? AND password=?";
        PreparedStatement preparedStatement=connection.prepareStatement(query);
        preparedStatement.setString(1,username);
        preparedStatement.setString(2,password);
        ResultSet rs = preparedStatement.executeQuery();
        if(rs.next())return true;
        return false;
    }*/
}
